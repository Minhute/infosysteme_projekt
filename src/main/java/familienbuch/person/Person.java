package familienbuch.person;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Person {

	//Attribute
	
//	@EmbeddedId					wahrscheinlich nicht n�tig mit self join
//	PersonBeziehungen id;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)		//id wird automatisch erwischen
	private int id;	
	private String firstname;
	private String lastname;
	private String salutation;
	private int phone;		//	darf nicht l�nger als 11 Zeichen sein
	private int mobile;			// darf nicht l�nger als x Zeichen sein
	private String mail;
	
	//Hier Join
	
	@ManyToMany
	@JoinTable(
			name = "Relationsship",
//			joinColumns = @JoinColumn(name = "id"),
//			inverseJoinColumns = @JoinColumn(name = "id"))
	private List<Person> personToPerson;

	//Konstruktoren

	public Person() {
	}

	public Person(int id, String firstname, String lastname, String salutation, int phone, int mobile, String mail) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.salutation = salutation;
		this.phone = phone;
		this.mobile = mobile;
		this.mail = mail;
	}
	
	//Methoden
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getfirstname() {
		return firstname;
	}

	public void setfirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getlastname() {
		return lastname;
	}

	public void setlastname(String lastname) {
		this.lastname = lastname;
	}

	public String getsalutation() {
		return salutation;
	}

	public void setsalutation(String salutation) {
		this.salutation = salutation;
	}

	public int getphone() {
		return phone;
	}

	public void setphone(int phone) {
		this.phone = phone;
	}

	public int getmobile() {
		return mobile;
	}

	public void setmobile(int mobile) {
		this.mobile = mobile;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public List<Person> getPersonToPerson() {
		return personToPerson;
	}

	public void setPersonToPerson(List<Person> personToPerson) {
		this.personToPerson = personToPerson;
	}
	
}
