package familienbuch.person;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
	@Autowired
	private PersonRepository personRepository;

	//hier Ausgabe der Personen
	public List<Person> getPersonList() {
		ArrayList<Person> myList = new ArrayList<>();
		Iterator<Person> it = personRepository.findAll().iterator();
		while (it.hasNext())
			myList.add(it.next());
		return myList;
	}
	
	//Basic commands
	public Person getPerson(int id) {
		return personRepository.findById(id).orElse(null);
	}

	public void addPerson(Person person) {
		personRepository.save(person);
	}

	public void updatePerson(int id, Person person) {
		personRepository.save(person);
	}

	public void deletePerson(int id, Person person) {
		personRepository.deleteById(id);
	}
}
