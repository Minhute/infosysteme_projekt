package familienbuch.person;

public class PersonController {
	package fh.aalen.person;

	import java.util.List;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestMethod;
	import org.springframework.web.bind.annotation.RestController;


		@RestController
		public class PersonController {
			
			@Autowired
			PersonService personService;
			
			@RequestMapping(method =RequestMethod.GET, value ="Noch ausf�llen")
			public List<Person> getAllPersons () {
				return personService.getAllPersons ();
			}
			
			@RequestMapping (method =RequestMethod.GET, value ="Noch ausf�llen" )
			public Person findById(@PathVariable int id) {
				return personService.findById(id);
			}
			@RequestMapping (method = RequestMethod.POST, value = "Noch ausf�llen")
			public void createPerson (@RequestBody Person person) {
				personService.createPerson(person);
			}
			
			@RequestMapping (method = RequestMethod.PUT, value ="Noch ausf�llen")
			public void updatePerson (@PathVariable int id, @RequestBody Person person) {
				personService.updatePerson(id, person);
			}

			@RequestMapping (method = RequestMethod.DELETE, value = "Noch ausf�llen")
			public void deleteById (@PathVariable int id) {
				personService.deleteById(id);	
			}
	}
}
